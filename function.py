import sys
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)
from populator import toggl, secrets_manager, dynamodb, org_chart


"""
AWS Lambda function which populates the Pulselive Knowledge Base DynamoDB Tables
"""


def lambda_handler(event, context):
    """
    Entry point for the lambda.
    :param event: aws event
    :param context: aws context
    """
    function_name = context.function_name
    function_stage = function_name.split('-')[0]

    toggl_token = secrets_manager.get_secret(function_stage)
    if not toggl_token:
        logging.error("Problem getting TOGGL_TOKEN from AWS Secrets Manager")
        sys.exit(1)

    logging.info("Getting Org Chart data from Heroku")
    org_chart_data = org_chart.get_org_chart_data()

    logging.info("Mapping Org Chart people to Toggl users")
    org_chart_data = toggl.map_toggl_users(org_chart_data, toggl_token)

    logging.info("Updating DynamoDB Table Based on Org Chart Data")
    for person, details in org_chart_data.items():
        logging.info(f"Processing {person}")
        dynamodb.insert_or_update_person(person, details, function_stage)
        toggl_data_for_user = toggl.get_toggl_data_for_user(details['toggl_uid'], toggl_token)
        logging.debug(toggl_data_for_user)
        dynamodb.update_person_toggl_data(details['toggl_uid'], toggl_data_for_user, function_stage)

    dynamodb.clean_up_person_table(org_chart_data, function_stage)


if __name__ == "__main__":
    class LambdaContext(object):
        def __init__(self):
            self.function_name = "dev"
    lambda_context = LambdaContext()
    lambda_handler("foo", lambda_context)
