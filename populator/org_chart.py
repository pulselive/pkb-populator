import json
import logging
import requests


def get_org_chart_data():
    org_chart = requests.get("https://jsoneditoronline.herokuapp.com/v1/docs/abf24482b2064155b48de2d928fc35e0")
    org_chart_json = json.loads(org_chart.text)
    org_chart_json_data = json.loads(org_chart_json['data'])
    flattened_org_chart_json_data = process_children(org_chart_json_data)
    logging.info("Found " + str(len(flattened_org_chart_json_data)) + " employees")
    return flattened_org_chart_json_data


def process_children(org_chart_json):
    employee_map = {org_chart_json['name']: {'title': org_chart_json['title']}}
    if 'children' in org_chart_json:
        for child in org_chart_json['children']:
            employee_map.update(process_children(child))
    return employee_map
