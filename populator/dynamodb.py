import logging

import boto3
import unidecode
from boto3.dynamodb.conditions import Attr


def insert_or_update_person(name, details, function_stage):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(function_stage + '-pkb-person')
    response = table.scan()
    items = response['Items']

    person_id = 0
    need_update = False
    for item in items:
        if item['person_name'] == name:
            person_id = item['person_id']
            if 'job_title' not in item or item['job_title'] != details['title']:
                need_update = True
            elif 'person_id' not in item or item['person_id'] != details['toggl_uid']:
                need_update = True
            elif 'toggl_email' not in item or item['toggl_email'] != details['toggl_email']:
                need_update = True

    if person_id == 0 or need_update:
        logging.debug(f"Adding or updating details for {name} to {str(details)}")
        table.put_item(
            Item={
                'person_id': details['toggl_uid'],
                'person_name': name,
                'job_title': details['title'],
                'toggl_email': details['toggl_email']
            }
        )
    else:
        logging.debug(f"Record for {name} already up to date")


def update_person_toggl_data(toggl_uid, toggl_data, function_stage):
    dynamodb = boto3.resource('dynamodb')
    project_table = dynamodb.Table(function_stage + '-pkb-project')
    project_person_table = dynamodb.Table(function_stage + '-pkb-project-person')

    for project, details in toggl_data.items():
        if 'client' not in details or 'project' not in details or 'project_colour' not in details or \
                not details['client'] or not details['project'] or not details['project_colour']:
            logging.warning(f"Skipping {project} with missing details details {str(details)}")
            continue
        project_response = project_table.get_item(
            Key={
                'project_id': project,
                'project_name': details['project'],
            }
        )
        project_search_name = unidecode.unidecode(details['client'] + " " + details['project']).lower()
        update_needed = False
        if 'Item' not in project_response:
            update_needed = True
        elif 'project_client' not in project_response['Item'] or (
                details['client'] != project_response['Item']['project_client']):
            update_needed = True
        elif 'project_name' not in project_response['Item'] or (
                details['project'] != project_response['Item']['project_name']):
            update_needed = True
        elif 'project_colour' not in project_response['Item'] or (
                details['project_colour'] != project_response['Item']['project_colour']):
            update_needed = True

        if update_needed:
            logging.debug(f"Adding or updating project to the project table with id {project}, name {details['project']} and colour {details['project_colour']}")
            project_table.put_item(
                Item={
                    'project_id': project,
                    'project_name': details['project'],
                    'project_client': details['client'],
                    'project_search_name': project_search_name,
                    'project_colour': details['project_colour'],
                }
            )

        logging.debug(
            f"Adding or updating time log info to the project person table with project id {project} and person_id {toggl_uid}")
        project_person_table.update_item(
            Key={
                'project_id': project,
                'person_id': toggl_uid
            },
            UpdateExpression="ADD time_logged :dur",
            ExpressionAttributeValues={
                ':dur': details['dur'],
            },
        )


def clean_up_person_table(flattened_org_chart_json_data, function_stage):
    logging.info("Cleaning up people not in org chart")
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(function_stage + '-pkb-person')
    response = table.scan()
    items = response['Items']
    project_person_table = dynamodb.Table(function_stage + '-pkb-person')

    for item in items:
        if item['person_name'] not in flattened_org_chart_json_data:
            logging.info(f"Deleting record for {item['person_name']} who is not in the Org Chart")
            table.delete_item(
                Key={
                    'person_id': item['person_id'],
                    'person_name': item['person_name']
                }
            )
            project_person_response = table.scan(FilterExpression=Attr("person_id").eq(item['person_id']))
            project_person_items = project_person_response['Items']
            logging.info(f"Deleting {len(project_person_items)} project person entries for {item['person_name']}")
            for project_person_item in project_person_items:
                project_person_table.delete_item(
                    Key={
                        'project_id': project_person_item['project_id'],
                        'person_id': project_person_item['person_id']
                    }
                )
