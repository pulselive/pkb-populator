import json

from boto3 import session
from botocore.exceptions import ClientError


def get_secret(function_stage):
    secret_name = function_stage + "-pkb-populator"
    region_name = "us-east-1"

    # Create a Secrets Manager client
    secrets_manager_session = session.Session()
    client = secrets_manager_session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        raise e
    else:
        if 'SecretString' in get_secret_value_response:
            json_secret_string = json.loads(get_secret_value_response['SecretString'])
            if 'TOGGL_TOKEN' in json_secret_string:
                return json_secret_string['TOGGL_TOKEN']

        return ""
