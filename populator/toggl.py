import json
import logging
import math
import sys
import datetime
import time

import requests
from requests.auth import HTTPBasicAuth


def map_toggl_users(flattened_org_chart_json_data, toggl_token):
    retries = 10
    retry_delay = 10
    while retries > 0:
        try:
            toggl_response = requests.get('https://api.track.toggl.com/api/v8/workspaces/464563/workspace_users?user_agent=pulselive-pkb-populator',
                                          auth=HTTPBasicAuth(toggl_token, 'api_token'),
                                          headers={'accept': 'application/json'})
            toggl_response.raise_for_status()
            break
        except requests.exceptions.RequestException as e:
            logging.error(f"Failed calling Toggl API for user list")
            logging.error(e)
            logging.info(f"Retrying in {retry_delay} seconds")
            time.sleep(retry_delay)
            retries -= 1

    if not toggl_response.ok:
        sys.exit(1)

    toggl_users_json = json.loads(toggl_response.text)
    no_match_count = 0

    resulting_dictionary = flattened_org_chart_json_data.copy()

    for org_chart_person in flattened_org_chart_json_data:
        logging.debug(f"Trying to map {org_chart_person} to a Toggl user")
        for toggl_user in toggl_users_json:
            if toggl_user['name'] == org_chart_person:
                toggl_email = toggl_user['email']
                toggl_uid = toggl_user['uid']
                resulting_dictionary[org_chart_person]['toggl_email'] = toggl_email
                resulting_dictionary[org_chart_person]['toggl_uid'] = toggl_uid
                logging.debug(f"Matched {org_chart_person} to a Toggl user")
                break

        if 'toggl_email' not in resulting_dictionary[org_chart_person]:
            logging.warning(f"{org_chart_person} was not matched to a Toggl user, trying fuzzy matching")
            org_chart_first_initial = org_chart_person[0].lower()
            org_chart_surname = org_chart_person.split(' ')[-1].lower()
            for toggl_user in toggl_users_json:
                if 'name' not in toggl_user or not toggl_user['name']:
                  logging.warning(f"Toggl user returned from API with no name")
                  continue
                toggl_name = toggl_user['name']
                toggl_first_initial = toggl_name[0].lower()
                toggl_surname = toggl_name.split(' ')[-1].lower()
                if org_chart_first_initial == toggl_first_initial and org_chart_surname == toggl_surname:
                    toggl_email = toggl_user['email']
                    toggl_uid = toggl_user['uid']
                    resulting_dictionary[org_chart_person]['toggl_email'] = toggl_email
                    resulting_dictionary[org_chart_person]['toggl_uid'] = toggl_uid
                    logging.info(f"Matched {org_chart_person} to a Toggl user {toggl_name} with fuzzy matching")
                    break

        if 'toggl_email' not in resulting_dictionary[org_chart_person]:
            logging.warning(f"{org_chart_person} was not matched to a Toggl user, they're dead to me. Removing from list.")
            del resulting_dictionary[org_chart_person]
            no_match_count += 1

    logging.warning(f"{str(no_match_count)} had no Toggl user")

    return resulting_dictionary


def get_toggl_data_for_user_api_call(toggl_uid, toggl_token, page):
    date_a_week_ago = str(datetime.date.today() - datetime.timedelta(days=7))

    retries = 10
    retry_delay = 10
    while retries > 0:
        try:
            response = requests.get(
                f'https://api.track.toggl.com/reports/api/v2/details?user_agent=pulselive-pkb-populator&workspace_id=464563&user_ids={toggl_uid}&since={date_a_week_ago}&page={page}',
                auth=HTTPBasicAuth(toggl_token, 'api_token'),
                headers={'accept': 'application/json'})
            response.raise_for_status()
            break
        except requests.exceptions.RequestException as e:
            logging.error(f"Failed calling Toggl API for UID {toggl_uid}")
            logging.error(e)
            logging.info(f"Retrying in {retry_delay} seconds")
            time.sleep(retry_delay)
            retries -= 1

    if not response.ok:
        sys.exit(1)
    else:
        logging.debug(f"Successfully made Toggl Report API call for user ID {toggl_uid} and page {page}")
        return json.loads(response.text)


def get_toggl_data_for_user(toggl_uid, toggl_token):
    project_time_dict = {}
    next_page = 1
    while next_page > 0:
        response_json = get_toggl_data_for_user_api_call(toggl_uid, toggl_token, next_page)
        next_page += 1
        total_count = response_json['total_count']
        per_page = response_json['per_page']

        if next_page > math.ceil(total_count / per_page):
            next_page = 0

        time_logs = response_json['data']
        for time_log in time_logs:
            pid = time_log['pid']
            dur = round(time_log['dur']/1000)
            client = time_log['client']
            project = time_log['project']
            project_colour = time_log['project_hex_color']
            if pid in project_time_dict:
                project_time_dict[pid]['dur'] += dur
            else:
                project_time_dict[pid] = {'dur': dur, 'client': client, 'project': project,
                                          'project_colour': project_colour}

    return project_time_dict
