# Pulselive Knowledge Base Populator

Contains the application and configuration for associated (DynamoDB) infrastucture for the Knowledge Base Populator.

This does the following jobs at the moment:

* Downloads the Pulselive Org Chart
* Combines that data with the list of Toggl users
* Gets time logs for each of those users for the last week
* Populates `person` and `project` DynamoDB tables with user and project details
* Populates a `project-person` table, **adding** the last week's duration on each project/person combination to what is already there

**Because of this behaviour it must only be run weekly**

Each environment has an entry of the same name in AWS Secrets Manager (currently existing for dev and production).

## Deploying

- Ensure you have the [Serverless Framework](https://www.serverless.com/framework/docs/providers/aws/guide/installation/) installed.
- Ensure you have Python 3 and pip installed
- Run `npm i`
- Run `sls deploy` to deploy to AWS, optionally with `--stage dev` to deploy to dev instead of production

